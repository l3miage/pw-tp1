<?php

class User
{
    public $nom;
    public $prenom;
    public $age;
    public $emailpublic;
    public $mdp;

    public function __construct()
    {
        $arguments = func_get_args();
        $numberOfArguments = func_num_args();

        if (method_exists($this, $function = '__construct' . $numberOfArguments)) {
            call_user_func_array(array($this, $function), $arguments);
        }
    }

    public function __construct5($i1, $i2, $i3, $i4, $i5)
    {
        $this->nom = $i1;
        $this->prenom = $i2;
        $this->age = $i3;
        $this->emailpublic = $i4;
        $this->mdp = $i5;
    }

    public function setNom($nom1)
    {
        if ($nom1 != 0) {
            $this->nom = $nom1;
        }
    }

    public function setPrenom($prenom1)
    {
        if ($prenom1 != 0) {
            $this->prenom = $prenom1;
        }
    }

    public function setAge($age1)
    {
        if ($age1 != 0) {
            $this->age = $age1;
        }
    }

    public function setEmail($emailpublic1)
    {
        if ($emailpublic1 != 0) {
            $this->emailpublic = $emailpublic1;
        }
    }

    public function setMdp($mdp1)
    {
        if ($mdp1 != 0) {
            $this->mdp = $mdp1;
        }
    }


    public function getNom()
    {
        return $this->nom;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getEmail()
    {
        return $this->emailpublic;
    }

    public function getMdp()
    {
        return $this->mdp;
    }


    public function vieillir($annee)
    {
        $age = $this->age + $annee;
    }

    public function valideMail()
    {
        if (filter_var($this->emailpublic, FILTER_VALIDATE_EMAIL)) {
            echo true;
        } else {
            echo false;
        }
    }


}