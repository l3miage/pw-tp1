<?php
if (isset($_GET['msg'])) {
    echo $_GET['msg'];
}
?>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600' rel='stylesheet' type='text/css'>
<link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
<link href="style.css" rel="stylesheet">

<div class="testbox">
    <h1>Créer un compte</h1>
    <form action="validation.php?acc=new" method="post">
        <label id="icon" for="mail"><i class="icon-envelope "></i></label>
        <input type="text" name="mail" id="mail" placeholder="Email" required/>

        <label id="icon" for="prenom"><i class="icon-coffee"></i></label>
        <input type="text" name="prenom" id="prenom" placeholder="Prenom" required/>

        <label id="icon" for="nom"><i class="icon-umbrella"></i></label>
        <input type="text" name="nom" id="nom" placeholder="Nom" required/>

        <label id="icon" for="age"><i class="icon-calendar"></i></label>
        <input type="text" name="age" id="age" placeholder="Age" required/>

        <label id="icon" for="password"><i class="icon-shield"></i></label>
        <input type="password" name="password" id="password" placeholder="Mot de passe" required/>

        <button type="submit" class="button">Register</button>
    </form>
</div>
<div class="testbox">
    <h1>Connexion</h1>
    <form action="validation.php" method="post">
        <label id="icon" for="mail"><i class="icon-envelope "></i></label>
        <input type="text" name="mail" id="mail" placeholder="Email" required/>

        <label id="icon" for="password"><i class="icon-shield"></i></label>
        <input type="password" name="password" id="password" placeholder="Mot de passe" required/>

        <button type="submit" class="button">Register</button>
    </form>
</div>