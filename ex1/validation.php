<?php
require './config.php';

require_once './user.php';

session_start();
$login = htmlspecialchars($_POST['mail']);
$mdp = htmlspecialchars($_POST['password']);

if (!empty($_GET['acc'])) {
    if (!empty($login) && !empty($mdp) && !empty($_POST['prenom']) && !empty($_POST['nom']) && !empty($_POST['age'])) {
        $user = new User($_POST['nom'],$_POST['prenom'],$_POST['age'],$login,$mdp);
        $_SESSION['username'] = $login;
        $_SESSION['connect'] = 'OK';
        header('Location:accueil.php');
    } else {
        header('Location:login.php?msg=Erreur 4 : Il manque une information pour la création du compte');
    }
} else {
    if (!empty($login) && !empty($mdp)) {
        if ($login == LEBONLOGIN && $mdp == LEBONPASS) {
            $_SESSION['username'] = $login;
            $_SESSION['connect'] = 'OK';
            header('Location:accueil.php');
        } else {
            header('Location:login.php?msg=Erreur 2 : Erreur de login/mot de passe');
        }
    } else {
        header('Location:login.php?msg=Erreur 1 : Veuillez saisir un login et un mot de passe');
    }
}

if (isset($_GET['afaire']) && $_GET['afaire'] === 'deconnexion') {
    session_destroy();
    header('Location:login.php?msg=Erreur 3 : Vous avez été déconnecté du service');
}